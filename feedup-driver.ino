/*
  feedup-driver

  This is the protocol handler and state machine for the feedup RS485
  8 channel mosfet driver module.
  
 */
#include <SoftwareSerial.h>
#include <EEPROMex.h>
#include <SimpleTimer.h>

#define ARRAYSIZE(arr) (sizeof(arr) / sizeof(arr[0]))

// Comment this line if debug is not desired.
#define DEBUG

#define CNTRL_485       A3
#define ENABLE_485_TX   1
#define ENABLE_485_RX   0
#define BAUD_RATE       115200

// Protocol constants
#define RSOF            0xf1
#define SOF1            0x00
#define SOF2            0xaa
#define SOF3            0x55
#define ACK             0x01
#define NACK            0x00


#define ERROR_LED       13

uint8_t pinMap[] = {2,3,4,5,6,7,8,9};
uint8_t num_pins = ARRAYSIZE(pinMap);
uint8_t buffer[256];
char log_buf[128];

/*
 * Command structures
 */
struct cmd_on_off {
  uint8_t command;
  uint8_t channel;
  uint16_t delay;
  uint16_t chksum;
};

/*
 * Protocol error codes
 */
enum error_codes_e {
  TRANSMISSION_OK = 0x00,
  SOF_ERROR,
  CHECKSUM_ERROR,
  UNSUPPORTED_COMMAND,
};

// Using a software serial channel for debug output.
SoftwareSerial mySerial(11, 12); // RX, TX

#if defined(DEBUG)
void log(const char *format, ...)
{
  va_list arglist;
  va_start(arglist, format);
  vsprintf(log_buf, format, arglist);
  mySerial.println(log_buf);
  va_end(arglist);
}
#else
#define log(...)
#endif

// EEPROM address placeholders
int start_range_addr;
int end_range_addr;

// caches for EEPROM values
int start_range;
int end_range;

// Data for defered processing
SimpleTimer timer;
#define NUM_TIMER_JOBS      10

struct TimerData {
  byte avail;
  int channel;
  int pin_data;
};
struct TimerData td[NUM_TIMER_JOBS];

// the setup function runs once when you press reset or power the board up
void setup() 
{
  uint8_t i;

  mySerial.begin(19200);
  Serial.begin(BAUD_RATE);
  mySerial.println(F("Starting command interpreter and logging !"));
  pinMode(CNTRL_485, OUTPUT);
  digitalWrite(CNTRL_485, ENABLE_485_RX);

  // Set all pins as output and reset them
  for(i=0;i<num_pins;i++) {
    pinMode(pinMap[i], OUTPUT);
    digitalWrite(pinMap[i], LOW);
  }

  // Initialize timer job data
  for (i=0;i<NUM_TIMER_JOBS;i++) {
    td[i].avail = true;
    td[i].channel = -1;
    td[i].pin_data = 0;
  }

  // Get EEPROM addresses
  start_range_addr = EEPROM.getAddress(sizeof(int));
  end_range_addr = EEPROM.getAddress(sizeof(int));

  // Get system settings and display them
  start_range = EEPROM.readInt(start_range_addr);
  end_range = EEPROM.readInt(end_range_addr);
  do_print_range();
}

/*
 * Find a free entry in the timer data array and return its index.
 * If no free entry is available then return -1
 */
int alloc_timer(void)
{
  int i;

  for (i=0;i<NUM_TIMER_JOBS;i++) {
    if (td[i].avail == true) {
      td[i].avail = false;
      log("Allocated timer %d", i);
      return i;
    }
  }
  return -1;
}

/*
 * Mark a timer data entry as free
 */
void free_timer(int tmr)
{
  td[tmr].avail = true;
  log("Freed timer %d", tmr);
}

/*
 * This is the timer callback. Here's were we update the specified pin
 */
void timeout(void* arg)
{
  byte tmr = (byte)arg;

  log("Timer Job %d fired, Channel %d data %d\n", tmr, td[tmr].channel, td[tmr].pin_data);
  digitalWrite(pinMap[td[tmr].channel], td[tmr].pin_data);
  free_timer(tmr);
}

/*
 * Update the timer data entry and start the timer
 */
void start_timer(int tmr, int time_out, int channel, int pin_data)
{
  td[tmr].channel = channel;
  td[tmr].pin_data = pin_data;

  timer.setTimeout(time_out, timeout, (void *)tmr);
}

/*
 * Perform a blocking read on the serial channel.
 * A timeout occurs after 100 ms
 */
int bRead()
{
  uint32_t tmr = millis();
  
  // Make sure the receiver path is enabled.
  digitalWrite(CNTRL_485, ENABLE_485_RX);
  
  // Wait for a character to arrive
  while(!Serial.available() && ((millis() - 100) < tmr));
  // Did we time out ?
  if ((millis() - 100) >= tmr) return -1;

  return Serial.read();
}

/*
 * execute pin command
 */
void pin_command(uint8_t channel, uint8_t cmd)
{
  struct cmd_on_off *coo = (struct cmd_on_off *)&buffer[0];

  // If the pin command has no delay we execute it immediatly
  if (!coo->delay) {
    digitalWrite(pinMap[channel], cmd);
  } else {
    // Here we queue the pin command for later execution.
    int tmr = alloc_timer();
    if (tmr < 0) {
      return;
    }
    start_timer(tmr, coo->delay, channel, cmd);
  }
}

/*
 * Send an acknowledge frame to the host
 */
void send_ack(uint8_t channel)
{
  // Set transceiver to TX
  digitalWrite(CNTRL_485, ENABLE_485_TX);
  Serial.write(RSOF);     // Send the start of response field
  Serial.write(ACK);      // Send ACK
  Serial.write(channel);  // The channel id
  Serial.write(0x00);
  Serial.flush();
  // Reset transceiver to RX
  digitalWrite(CNTRL_485, ENABLE_485_RX);
  log("ACK response sent");
}

/*
 * This function sends a NACK to the host. A delay can be 
 * specified by the caller to ensure that the host has finished
 * transmitting all data before responding with the NACK.
 */
void send_nack(uint8_t channel, uint8_t error, uint16_t dly)
{
  if (dly) {
    delay(delay);
  }
  // Set transceiver to TX
  digitalWrite(CNTRL_485, ENABLE_485_TX);
  Serial.write(RSOF);     // Send the start of response field
  Serial.write(NACK);     // Send NACK
  Serial.write(channel);  // The channel id
  Serial.write(error);
  Serial.flush();
  // Reset transceiver to RX
  digitalWrite(CNTRL_485, ENABLE_485_RX);
  log("NACK response sent, error %d", error);
}

static char cmdBuf[32];
static byte cmdPtr = 0;
static char delSeq[] = {8, 32, 8, 0};

// This is the start range command
void do_start_range(void) 
{
  int p;

  p = (int)strtol(&cmdBuf[2], NULL, 10);
  mySerial.print(F("The address range now starts at: "));
  mySerial.println(p);
  start_range = p;
  EEPROM.writeInt(start_range_addr, start_range);
}

// This is the end range command
void do_end_range(void) 
{
  int p;

  p = (int)strtol(&cmdBuf[2], NULL, 10);
  mySerial.print(F("The address range now ends at: "));
  mySerial.println(p);
  end_range = p;
  EEPROM.writeInt(end_range_addr, end_range);
}

// This is the print ranges command
void do_print_range(void)
{
  mySerial.print(F("Address range start: "));
  mySerial.println(start_range);
  mySerial.print(F("Address range end: "));
  mySerial.println(end_range);
}

// Execute the command found in cmdBuf
// The command is composed as follows:
//   <c>:<p>
// where c is a one letter command and p is a numeric parameter.
// no other syntaxes is allowed and the parser is not very good
// at detecting errors so beware.
void do_command(void)
{
  // First check that the delimiter seem ok
  if (cmdBuf[1] != ':' && cmdBuf[1] != 0) {
    mySerial.println(F("Incorrect delimiter found."));
    return;
  }

  // Now try to decode the command
  switch(cmdBuf[0])
  {
    case 's':     // This is the set start address range command
      do_start_range();
      break;

    case 'e':     // This is the set end address range command
      do_end_range();
      break;

    case 'p':
      do_print_range();
      break;

    default:
      mySerial.println(F("Invalid command found."));
      return;
      break;
  }
}

// the loop function runs over and over again forever
void loop() 
{
  uint8_t i;
  uint16_t checksum;
  uint8_t channel;

  timer.run();
  
  // Take care of the parameter settings interpreter.
  if (mySerial.available()) {
    char ch = mySerial.read();
    if (ch == 127) {
      if (cmdPtr) {
        cmdPtr--;
        cmdBuf[cmdPtr] = 0;
        mySerial.print(delSeq);
      }
    } else if (ch == 13) {
      mySerial.write(13);
      mySerial.write(10);
      cmdBuf[cmdPtr] = 0;
      do_command();
      cmdPtr = 0;
    } else {
      cmdBuf[cmdPtr++] = ch;
      mySerial.write(ch);
    }
  }

  // NOTE: Do not enter debug logs after this point. Software
  // serial breaks the timing and the protocol breaks.

  // Make sure we sync to a SOF
  // The only acceptable return value here are the 2 first bytes of the SOF.
  if (bRead() != SOF1) return;
  if (bRead() != SOF2) return;
  // And check the final SOF byte
  if (bRead() == SOF3) {
    // This is were we end up if a SOF is detected.
    checksum = 0xff; // This is the sum of all SOF bytes, 0x00 + 0xaa + 0x55
    uint8_t length = bRead();  // Get length of frame but dont store it
    checksum += length;
    for(i=0;i<length-3;i++) {
      buffer[i] = bRead();
      checksum += buffer[i];
    }
    // Add the checksum from the protocol
    checksum += bRead() + bRead() * 256;
    // Make sure the checksum is correct, otherwise report to host.
    if (checksum) {
      send_nack(0, CHECKSUM_ERROR, 1);
      log("Checksum missmatch (0x%04x)", checksum);
      return;
    }
    if (buffer[1] >= start_range && buffer[1] <= end_range) {
      channel = buffer[1] - start_range;
      // Command executioner
      switch(buffer[0]) {
        case 0x00:  // NOP
          break;

        case 0x01:  // Set channel
        {
          pin_command(channel, HIGH);
          break;
        }

        case 0x02:  // Clear channel
          pin_command(channel, LOW);
          break;

        case 0x03:  // Clear all channels
          for(i=0;i<num_pins;i++) {
            digitalWrite(pinMap[i], LOW);
          }
          // Only the device that holds the first channel shall respond to this
          if (start_range != 0)
            return;
          break;

        default:
          send_nack(0, UNSUPPORTED_COMMAND, 1);
          return;
          break;
      }
      send_ack(channel);
    }
  }
}
