#!/usr/bin/python

import sys
import time
import serial
import string


args = len(sys.argv)
if args != 4:
  print "Invalid number of arguments."
  print "  You should have typed something like this 'tests /dev/ttyS2'"
  sys.exit()

port = sys.argv[1]
print "Using port", port
command = sys.argv[2]
channel = sys.argv[3]

ser = serial.serial_for_url(port, do_not_open=True)
ser.baudrate = 115200
ser.rtscts = False
ser.xonxoff = False
ser.open()
#ser.setDTR(0)
#ser.setRTS(0)

#                 SOF       | len | cmd | chan|    delay  |  chksum
elements = [0x00, 0xaa, 0x55, 0x07, 0x01, 0x04, 0x00, 0x00]
txvals = bytearray(elements)
txvals[4] = int(command)
txvals[5] = int(channel)

# Get twos complemented checksum
chksum = abs(~sum(txvals) & 0xffff) + 1

# Append the checksum
txvals.append(chksum & 0xff)
txvals.append(chksum >> 8)

ser.write(txvals)
ser.flush()

ser.timeout = 0.5
getresult = ser.read()
if len(getresult) != 0:
  result = ord(getresult)
  if result != 0xf1:
    print "Incorrect RSOF, was %x, should be 0xf1. Terminating transmission" % result
  else:
    result = ord(ser.read())
    channel = ord(ser.read())
    errcode = ord(ser.read())
    if result == 1:
      print "Successfull transmission on channel %d" % channel
    else:
      print "Unsuccessful transmission, error code %d" % errcode
else:
  print "No slave device was found that could service your request"

ser.close()
